import "./App.css";
import MyForm from "./components/MyForm1";
import MyFormValidateField from "./components/MyForm10ValidateField";
import MyFormValidation from "./components/MyForm2Validation";
import MyFormValidationRefactored from "./components/MyForm2_1Validation";
import MyFormValidationError from "./components/MyForm3DisplayError";
import MyFormValidateVisitedField from "./components/MyForm3_1DisplayError";
import MyFormValidateUsingYup from "./components/MyForm4ValidationUsingYup";
import MyFormReduceBoilerPlate from "./components/MyForm5ReduceBoilerPlate";
import MyFormReduceBoilerPlateUsingFormikComponents from "./components/MyForm5_1FormikComponent";
import MyFormFieldComponent from "./components/MyForm6FieldComponent";
import MyFormErrorMessageComponent from "./components/MyForm6_1ErrorMessageComponent";
import MyFormNestedObject from "./components/MyForm7NestedObject";
import MyFormDataArray from "./components/MyForm7_1Array";
import MyFormDynamicForm from "./components/MyForm8DynamicForm";
import MyFormControlValidation from "./components/MyForm9ControlValidation";
import MyFormTriggerValidation from "./components/MyForm11TriggerValidation";
import MyFormDisableSubmit1 from "./components/MyForm12_1DisableSubmit";
import MyFormDisableSubmit2 from "./components/MyForm12_2DisableSubmit";
import MyFormLoadSaveData from "./components/MyForm13LoadSaveData";
import MyFormResetData from "./components/MyForm14ResetFormData";
import SelectCheckboxDropdown from "./Practice/SelectCheckboxDropdown";
import FormikContainer from "./ReusableFormControl/FormikContainer";
import LoginForm from "./ReusableFormControl/Form/LoginForm";

function App() {
  return (
    <div className="App">
      {/* <MyForm /> */}
      {/* <MyFormValidation /> */}
      {/* <MyFormValidationRefactored /> */}
      {/* <MyFormValidationError /> */}
      {/* <MyFormValidateVisitedField /> */}
      {/* <MyFormValidateUsingYup /> */}
      {/* <MyFormReduceBoilerPlate /> */}
      {/* <MyFormReduceBoilerPlateUsingFormikComponents /> */}

      {/* <MyFormFieldComponent /> */}
      {/* <MyFormErrorMessageComponent /> */}
      {/* <MyFormNestedObject /> */}
      {/* <MyFormDataArray /> */}
      {/* <MyFormDynamicForm /> */}
      {/* <MyFormControlValidation /> */}
      {/* <MyFormValidateField /> */}
      {/* <MyFormTriggerValidation /> */}
      {/* <MyFormDisableSubmit1 /> */}
      {/* <MyFormDisableSubmit2 /> */}
      {/* <MyFormLoadSaveData /> */}
      {/* <MyFormResetData /> */}

      {/* <FormikContainer /> */}
      <LoginForm />

      {/* <SelectCheckboxDropdown /> */}
    </div>
  );
}

export default App;
