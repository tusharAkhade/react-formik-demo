import React from "react";

const TextError = (props) => {
  return <div className="error-field">{props.children}</div>;
};

export default TextError;
