import { ErrorMessage, Field, Form, Formik } from "formik";
import React from "react";
import * as Yup from "yup";

const initialValues = {
  name: "",
  email: "",
  username: "",
};

const onSubmit = (values) => {
  console.log("Form data : ", values);
};

const validationSchema = Yup.object({
  name: Yup.string().required("Cannot empty."),
  email: Yup.string().email("Invalid format").required("Cannot empty."),
  username: Yup.string().required("Cannot empty."),
});

// Reducing the boilerplate using 'Formik', 'Form' and 'ErrorMessage' component
function MyFormReduceBoilerPlateUsingFormikComponents() {
  return (
    // Wrapped form with 'Formik' component and removed 'useFormik()' hook
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      {/* replace 'form' tag with formik 'Form' component and removed onSubmit of form tag */}
      <Form>
        <div className="form-control">
          <label htmlFor="name">Name</label>
          {/* replace 'input' tag with 'Field' component */}
          {/* 'Field' component will 
                1. hookup inputs to the top-level 'Formik' component 
                2. uses 'name' attribute to match up the formik state
                3. by default 'Field' component will render an input element
          */}
          <Field type="text" id="name" name="name" />
          {/* 'ErrorMessage' component will take care of rendering error message for particular field,
               indicated by 'name' prop, only if the field has been visited and if there exists
          */}
          <ErrorMessage name="name" />
        </div>

        <div className="form-control">
          <label htmlFor="email">Email</label>
          {/* replace 'input' tag with 'Field' component */}
          <Field type="email" id="email" name="email" />
          <ErrorMessage name="email" />
        </div>

        <div className="form-control">
          <label htmlFor="username">Username</label>
          {/* replace 'input' tag with 'Field' component */}
          <Field type="text" id="username" name="username" />
          <ErrorMessage name="username" />
        </div>

        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
}

export default MyFormReduceBoilerPlateUsingFormikComponents;
