import { useFormik } from "formik";
import React from "react";

function MyFormValidation() {
  const formik = useFormik({
    // Note: Properties for "initialValues" correspond to the name attribute of the individual fields
    initialValues: {
      name: "",
      email: "",
      username: "",
    },

    onSubmit: (values) => {
      console.log("Form data : ", values);
    },

    //'validate()' has 3 condition
    // 1. must return an object
    // 2. the keys of that object should be similar to the name attribute for the form fields
    //    or we can say, it is similar to the 'initialValues' object
    // 3. error message assigned to the key is should be 'string'
    validate: (values) => {
      // Condition 2 => errors.name errors.email errors.username
      let errors = {};

      if (!values.name) {
        errors.name = "Cannot be empty"; // Condition 3
      }

      if (!values.email) {
        errors.email = "Cannot be empty"; // Condition 3
      } else if (
        !/^$[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i.test(values.email)
      ) {
        errors.email = "Invalid email format"; // Condition 3
      }
      if (!values.username) {
        errors.username = "Cannot be empty"; // Condition 3
      }

      return errors; // Condition 1
    },
  });

  //   console.log(formik);

  //   console.log(formik.values);

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <div>
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            onChange={formik.handleChange}
            value={formik.values.name}
          />
        </div>
        <div>
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            onChange={formik.handleChange}
            value={formik.values.email}
          />
        </div>
        <div>
          <label htmlFor="username">Username</label>
          <input
            type="text"
            id="username"
            name="username"
            onChange={formik.handleChange}
            value={formik.values.username}
          />
        </div>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default MyFormValidation;
