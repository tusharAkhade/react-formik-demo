import { useFormik } from "formik";
import React from "react";
import * as Yup from "yup";

const initialValues = {
  name: "",
  email: "",
  username: "",
};

const onSubmit = (values) => {
  console.log("Form data : ", values);
};

const validationSchema = Yup.object({
  name: Yup.string().required("Cannot empty."),
  email: Yup.string().email("Invalid format").required("Cannot empty."),
  username: Yup.string().required("Cannot empty."),
});

// Form validation using 'Yup'
function MyFormReduceBoilerPlate() {
  const formik = useFormik({
    // Note: Properties for "initialValues" correspond to the name attribute of the individual fields
    initialValues,
    onSubmit,
    validationSchema,
  });

  console.log("Visited fields: ", formik.touched); // 'touched' will stores visited fields

  return (
    <div>
      <h2>Validation using Yup</h2>
      <form onSubmit={formik.handleSubmit}>
        <div className="form-control">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            // 'getFieldProps' property
            {...formik.getFieldProps("name")} // passing 'name' attribute to the 'getFieldProps()' method
          />
          {formik.touched.name && formik.errors.name ? (
            <div className="error-field">{formik.errors.name}</div>
          ) : null}
        </div>

        <div className="form-control">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            // 'getFieldProps' property
            {...formik.getFieldProps("email")} // passing 'name' attribute to the 'getFieldProps()' method
          />
          {formik.touched.email && formik.errors.email ? (
            <div className="error-field">{formik.errors.email}</div>
          ) : null}
        </div>

        <div className="form-control">
          <label htmlFor="username">Username</label>
          <input
            type="text"
            id="username"
            name="username"
            // 'getFieldProps' property
            {...formik.getFieldProps("username")} // passing 'name' attribute to the 'getFieldProps()' method
          />
          {formik.touched.username && formik.errors.username ? (
            <div className="error-field">{formik.errors.username}</div>
          ) : null}
        </div>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default MyFormReduceBoilerPlate;
