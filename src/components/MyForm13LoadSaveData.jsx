import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import TextError from "../ui/TextError";

const initialValues = {
  name: "",
  email: "",
  username: "",
  comment: "",
};

// Step 1
const savedData = {
  name: "Name",
  email: "email@gmail.com",
  username: "username",
  comment: "comment",
};

const onSubmit = (values, onSubmitProps) => {
  console.log("Form data : ", values);
  console.log("Submit props: ", onSubmitProps);
  onSubmitProps.setSubmitting(false);
};

const validationSchema = Yup.object({
  name: Yup.string().required("Cannot empty."),
  email: Yup.string().email("Invalid format").required("Cannot empty."),
  username: Yup.string().required("Cannot empty."),
});

function MyFormLoadSaveData() {
  const [formValues, setFormValues] = useState(null);
  return (
    <Formik
      initialValues={formValues || initialValues} // step 3: use 'formValues' is present or else use 'initialValues'
      validationSchema={validationSchema}
      onSubmit={onSubmit}
      enableReinitialize={true} // step 3 imp: enable the reinitialization
    >
      {(formik) => {
        console.log("Formik props : ", formik);
        return (
          <Form>
            <h2>Load the saved data.</h2>
            <div className="form-control">
              <label htmlFor="name">Name</label>
              <Field
                type="text"
                placeholder="Enter your name"
                id="name"
                name="name"
              />
              <ErrorMessage name="name" component={TextError} />
            </div>

            <div className="form-control">
              <label htmlFor="email">Email</label>
              <Field type="email" id="email" name="email" />
              <ErrorMessage name="email">
                {(errorMessage) => {
                  return <div className="error-field">{errorMessage}</div>;
                }}
              </ErrorMessage>
            </div>

            <div className="form-control">
              <label htmlFor="username">Username</label>
              <Field type="text" id="username" name="username" />
              <div className="error-field">
                <ErrorMessage name="username" />
              </div>
            </div>

            <div className="form-control">
              <label htmlFor="comment">Comment</label>
              <Field as="textarea" id="comment" name="comment" />
              <ErrorMessage name="comment" component={TextError} />
            </div>

            {/* Step 2: set the Form values */}
            <button type="button" onClick={() => setFormValues(savedData)}>
              Load save Data
            </button>

            <button type="submit" disabled={formik.isSubmitting}>
              Submit
            </button>
          </Form>
        );
      }}
    </Formik>
  );
}

export default MyFormLoadSaveData;
