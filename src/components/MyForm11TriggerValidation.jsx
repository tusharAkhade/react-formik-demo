import { ErrorMessage, Field, Form, Formik } from "formik";
import React from "react";
import * as Yup from "yup";
import TextError from "../ui/TextError";

const initialValues = {
  name: "",
  email: "",
  username: "",
  comment: "",
};

const onSubmit = (values) => {
  console.log("Form data : ", values);
};

const validationSchema = Yup.object({
  name: Yup.string().required("Cannot empty."),
  email: Yup.string().email("Invalid format").required("Cannot empty."),
  username: Yup.string().required("Cannot empty."),
});

const validateComment = (value) => {
  let err;
  if (!value) {
    err = "Error message";
  }
  return err;
};

function MyFormTriggerValidation() {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      {(formik) => {
        console.log("Formik props : ", formik);
        return (
          <Form>
            <h2>
              Validating comment after clicking on Validate comment button &
            </h2>
            <h2>Validating all fields after clicking on Validate All button</h2>
            <div className="form-control">
              <label htmlFor="name">Name</label>
              <Field
                type="text"
                placeholder="Enter your name"
                id="name"
                name="name"
              />
              <ErrorMessage name="name" component={TextError} />
            </div>

            <div className="form-control">
              <label htmlFor="email">Email</label>
              <Field type="email" id="email" name="email" />
              <ErrorMessage name="email">
                {(errorMessage) => {
                  return <div className="error-field">{errorMessage}</div>;
                }}
              </ErrorMessage>
            </div>

            <div className="form-control">
              <label htmlFor="username">Username</label>
              <Field type="text" id="username" name="username" />
              <div className="error-field">
                <ErrorMessage name="username" />
              </div>
            </div>

            <div className="form-control">
              <label htmlFor="comment">Comment</label>
              <Field
                as="textarea"
                id="comment"
                name="comment"
                validate={validateComment}
              />
              <ErrorMessage name="comment" component={TextError} />
            </div>

            <button
              type="button"
              onClick={() => formik.validateField("comment")}
            >
              Validate comment
            </button>
            <button type="button" onClick={() => formik.validateForm()}>
              Validate All
            </button>

            <button
              type="button"
              onClick={() => formik.setFieldTouched("comment")}
            >
              Visit comment
            </button>
            <button
              type="button"
              onClick={() =>
                formik.setTouched({
                  name: true,
                  email: true,
                  username: true,
                  comment: true,
                })
              }
            >
              Visit All Fields
            </button>

            <button type="submit">Submit</button>
          </Form>
        );
      }}
    </Formik>
  );
}

export default MyFormTriggerValidation;
