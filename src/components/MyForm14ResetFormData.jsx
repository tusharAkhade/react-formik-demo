import { ErrorMessage, Field, Form, Formik } from "formik";
import React from "react";
import * as Yup from "yup";
import TextError from "../ui/TextError";

const initialValues = {
  name: "",
  email: "",
  username: "",
};

const onSubmit = (values, onSubmitProps) => {
  console.log("Form data : ", values);
  console.log("Submit props: ", onSubmitProps);
  onSubmitProps.resetForm(); // it will reset the form
};

const validationSchema = Yup.object({
  name: Yup.string().required("Cannot empty."),
  email: Yup.string().email("Invalid format").required("Cannot empty."),
  username: Yup.string().required("Cannot empty."),
});

function MyFormResetData() {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      {(formik) => {
        console.log("Formik props : ", formik);
        return (
          <Form>
            <h2>Reset form after submitted.</h2>
            <div className="form-control">
              <label htmlFor="name">Name</label>
              <Field
                type="text"
                placeholder="Enter your name"
                id="name"
                name="name"
              />
              <ErrorMessage name="name" component={TextError} />
            </div>

            <div className="form-control">
              <label htmlFor="email">Email</label>
              <Field type="email" id="email" name="email" />
              <ErrorMessage name="email">
                {(errorMessage) => {
                  return <div className="error-field">{errorMessage}</div>;
                }}
              </ErrorMessage>
            </div>

            <div className="form-control">
              <label htmlFor="username">Username</label>
              <Field type="text" id="username" name="username" />
              <div className="error-field">
                <ErrorMessage name="username" />
              </div>
            </div>

            <button type="submit" disabled={formik.isSubmitting}>
              Submit
            </button>
          </Form>
        );
      }}
    </Formik>
  );
}

export default MyFormResetData;
