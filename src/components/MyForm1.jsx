import { useFormik } from "formik";
import React from "react";

function MyForm() {
  const formik = useFormik({
    // Note: Properties for "initialValues" correspond to the name attribute of the individual fields
    initialValues: {
      name: "",
      email: "",
      username: "",
    },

    onSubmit: (values) => {
      console.log("Form data : ", values);
    },
  });

  //   console.log(formik);

  //   console.log(formik.values);

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <div>
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            onChange={formik.handleChange}
            value={formik.values.name}
          />
        </div>
        <div>
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            onChange={formik.handleChange}
            value={formik.values.email}
          />
        </div>
        <div>
          <label htmlFor="username">Username</label>
          <input
            type="text"
            id="username"
            name="username"
            onChange={formik.handleChange}
            value={formik.values.username}
          />
        </div>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default MyForm;
