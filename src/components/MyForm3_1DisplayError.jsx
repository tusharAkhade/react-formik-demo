import { useFormik } from "formik";
import React from "react";

const initialValues = {
  name: "",
  email: "",
  username: "",
};

const onSubmit = (values) => {
  console.log("Form data : ", values);
};

const validate = (values) => {
  let errors = {};

  if (!values.name) {
    errors.name = "Cannot be empty";
  }

  if (!values.email) {
    errors.email = "Cannot be empty";
  } else if (!/^$[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i.test(values.email)) {
    errors.email = "Invalid email format";
  }

  if (!values.username) {
    errors.username = "Cannot be empty";
  }

  return errors;
};

function MyFormValidateVisitedField() {
  const formik = useFormik({
    // Note: Properties for "initialValues" correspond to the name attribute of the individual fields
    initialValues,
    onSubmit,
    validate,
  });

  console.log("Visited fields: ", formik.touched); // 'touched' will stores visited fields

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <div className="form-control">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur} // 'formik.handleBlur' will store the information whether the field visitd or not and that will store on 'toched' property.
            value={formik.values.name}
          />
          {formik.touched.name && formik.errors.name ? (
            <div className="error-field">{formik.errors.name}</div>
          ) : null}
        </div>

        <div className="form-control">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur} // 'formik.handleBlur' will store the information whether the field visitd or not and that will store on 'toched' property.
            value={formik.values.email}
          />
          {formik.touched.email && formik.errors.email ? (
            <div className="error-field">{formik.errors.email}</div>
          ) : null}
        </div>

        <div className="form-control">
          <label htmlFor="username">Username</label>
          <input
            type="text"
            id="username"
            name="username"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur} // 'formik.handleBlur' will store the information whether the field visitd or not and that will store on 'toched' property.
            value={formik.values.username}
          />
          {formik.touched.username && formik.errors.username ? (
            <div className="error-field">{formik.errors.username}</div>
          ) : null}
        </div>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default MyFormValidateVisitedField;
