import { ErrorMessage, Field, Form, Formik } from "formik";
import React from "react";
import * as Yup from "yup";
import TextError from "../ui/TextError";

const initialValues = {
  name: "",
  email: "",
  username: "",
  comment: "",
  address: "",
  social: {
    facebook: "",
    twitter: "",
  },
};

const onSubmit = (values) => {
  console.log("Form data : ", values);
};

const validationSchema = Yup.object({
  name: Yup.string().required("Cannot empty."),
  email: Yup.string().email("Invalid format").required("Cannot empty."),
  username: Yup.string().required("Cannot empty."),
});

// Add nested objects
function MyFormNestedObject() {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <div className="form-control">
          <label htmlFor="name">Name</label>
          {/* any additional prop in the 'Field' component will be passed through */}
          <Field
            type="text"
            placeholder="Enter your name"
            id="name"
            name="name"
          />
          {/* 'component' prop will warapped error message with the tag/component */}
          <ErrorMessage name="name" component={TextError} />
        </div>

        <div className="form-control">
          <label htmlFor="email">Email</label>
          <Field type="email" id="email" name="email" />
          {/* 'component' prop will warapped error message with the tag/component */}
          <ErrorMessage name="email">
            {(errorMessage) => {
              return <div className="error-field">{errorMessage}</div>;
            }}
          </ErrorMessage>
        </div>

        <div className="form-control">
          <label htmlFor="username">Username</label>
          <Field type="text" id="username" name="username" />
          <div className="error-field">
            <ErrorMessage name="username" />
          </div>
        </div>

        <div className="form-control">
          <label htmlFor="comment">Comment</label>
          {/* 'Field' component can render different element other than input element */}
          <Field as="textarea" id="comment" name="comment" />
        </div>

        <div className="form-control">
          <label htmlFor="address">Address</label>
          <Field name="address">
            {(props) => {
              console.log(props);
              const { field, form, meta } = props;
              return (
                <div>
                  <input type="text" id="address" {...field} />
                  {meta.touched && meta.error ? <div>{meta.error}</div> : null}
                </div>
              );
            }}
          </Field>
        </div>

        <div className="form-control">
          <label htmlFor="facebook">Facebook</label>
          <Field type="text" id="facebook" name="social.facebook" />
        </div>

        <div className="form-control">
          <label htmlFor="twitter">Twitter</label>
          <Field type="text" id="twitter" name="social.twitter" />
        </div>

        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
}

export default MyFormNestedObject;
