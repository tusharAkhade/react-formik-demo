import InputField from "./InputField";
import RadioField from "./RadioField";
import SelectField from "./SelectField";
import TextareaField from "./TextareaField";
import CheckboxGroupField from "./CheckboxGroupField";
import DatePicker from "./DatePicker";

const FormikControl = (props) => {
  const { control, ...rest } = props;
  switch (control) {
    case "input":
      return <InputField {...rest} />;
    case "textarea":
      return <TextareaField {...rest} />;
    case "select":
      return <SelectField {...rest} />;
    case "radio":
      return <RadioField {...rest} />;
    case "checkbox":
      return <CheckboxGroupField {...rest} />;
    case "date":
      return <DatePicker {...rest} />;
    default:
      return null;
  }
};

export default FormikControl;
