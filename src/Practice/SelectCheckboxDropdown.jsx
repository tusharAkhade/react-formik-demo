import React, { useState, useReducer, useRef, useEffect } from "react";

const options = ["abc", "xyz", "def", "asd", "ssdd"];

const initialState = {
  filterNames: options,
  selections: [],
};
const reducer = (state, action) => {
  switch (action.type) {
    case "filter":
      return {
        ...state,
        filterNames: options.filter(
          (option) =>
            option.toLowerCase().indexOf(action.payload.toLowerCase()) > -1
        ),
      };

    case "checked":
      return { ...state, selections: [...state.selections, action.payload] };

    case "unChecked":
      return {
        ...state,
        selections: state.selections.filter(
          (option) => option !== action.payload
        ),
      };

    default:
      return state;
  }
};

// unchecked the values on search: fixed by adding 'checked' attribute in the checkbox
// checked valued add to the input: done
// hide checkbox options: fixed by using useEffect
// //   setState lag: bug fixed by using useReducer hook instead of useState
const SelectCheckboxDropdown = () => {
  const [isOpen, setIsOpen] = useState(false);

  const [state, dispatch] = useReducer(reducer, initialState);
  console.log("States in useReducers", state);

  const handleCheckbox = (e) => {
    if (e.target.checked) {
      dispatch({ type: "checked", payload: e.target.value });
    } else {
      dispatch({ type: "unChecked", payload: e.target.value });
    }
  };

  const handleInput = (e) => {
    dispatch({ type: "filter", payload: e.target.value });
  };

  const wrapperDivRef = useRef();

  useEffect(() => {
    function handleClickOutside(e) {
      if (wrapperDivRef.current && !wrapperDivRef.current.contains(e.target)) {
        setIsOpen(false);
        console.log("Clicked outside the div");
      }
    }
    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [wrapperDivRef]);

  return (
    <>
      <div ref={wrapperDivRef}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            border: "1px solid",
            width: "600px",
          }}
          data-placeholder="You can type here."
        >
          {state.selections?.map((option) => (
            <div
              style={{
                marginRight: "10px",
                border: "1px solid",
                borderRadius: "20px",
                padding: "5px",
                background: "lightgrey",
                width: "80px",
              }}
            >
              <span style={{ cursor: "default" }} contentEditable="false">
                {option}
              </span>{" "}
              <strong
                style={{ cursor: "pointer" }}
                onClick={() => dispatch({ type: "unChecked", payload: option })}
              >
                X
              </strong>
            </div>
          ))}
          <input
            type="text"
            onChange={handleInput}
            onFocus={() => setIsOpen(true)}
            style={{ border: "none", outline: "none", paddingLeft: "0px" }}
          />
        </div>
        <div
          style={{
            maxHeight: "100px",
            border: "1px solid ",
            overflowY: "scroll",
            width: "100%",
            display: `${isOpen ? "block" : "none"}`,
          }}
        >
          {state?.filterNames?.map((option) => (
            <div key={option} style={{ display: "flex" }}>
              <input
                type="checkbox"
                name={option}
                value={option}
                id={`selectOption-${option}`}
                onChange={handleCheckbox}
                checked={state.selections.includes(option)} // imp, otherwise selected checkbox will be unchecked checkbox after searching
              />
              <label
                style={{ width: "100%" }}
                htmlFor={`selectOption-${option}`}
              >
                {option}
              </label>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default SelectCheckboxDropdown;
